class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operation(:+)
  end

  def value
    @stack.last
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(string)
    token_s = string.split
    token_s.map do |token|
      is_operand?(token) ? token.to_sym : token.to_i
    end
  end

  def evaluate(string)
    token_s = tokens(string)

    token_s.each do |token|
      case token
      when Integer
        push(token)
      when Symbol
        perform_operation(token)
      end
    end

    value
  end

  private

  def perform_operation(op_symbol)

    check_error

    second_operand = @stack.pop
    first_operand = @stack.pop

    case op_symbol
    when :+
      @stack << second_operand + first_operand
    when :*
      @stack << second_operand * first_operand
    when :-
      @stack << first_operand - second_operand
    when :/
      @stack << first_operand.to_f / second_operand.to_f
    end
  end

  def check_error
    raise "calculator is empty" if @stack.size < 2
  end

  def is_operand?(char)
    ("+-*/").include?(char)
  end
end
